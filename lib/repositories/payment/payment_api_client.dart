import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:meta/meta.dart';
import 'package:http/http.dart' as http;

class PaymentApiClient {
  final http.Client httpClient;

  PaymentApiClient({@required this.httpClient}) : assert(httpClient != null);

  Future generatePayment(var body) async {
    String url = 'https://ecommerce.amipass.com/amipayApi/generate_payment.php';
    try {
      final response = await http.post(url, body: json.encode(body), headers: {"Content-Type": "application/json"});
      var resp = json.decode(response.body);
      if (resp['data']['codigo'].toString() == "1") {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print(e);
      return false;
    }
  }

  Future<String> getUserData(String userID) async {
    String url = 'https://ecommerce.amipass.com/amipayApi/get_empresa.php?id=$userID';
    try {
      final response = await http.get(url, headers: {"Content-Type": "application/json"});
      if (response.statusCode == 200) {
        var resp = json.decode(response.body);
        return resp['data']['sEmpresa'];
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future getCommerceData(String codigoPay) async {
    String url = 'https://ecommerce.amipass.com/amipayApi/get_commerce_data.php?id=$codigoPay';
    try {
      final response = await http.get(url, headers: {"Content-Type": "application/json"});

      if (response.statusCode == 200) {
        var resp = json.decode(response.body);

        var respuesta = {"comercio": resp['data']['sComercio'], "local": resp['data']['sLocal']};
        return respuesta;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      return null;
    }
  }
}
