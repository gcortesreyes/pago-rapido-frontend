import 'package:amipay/repositories/payment/payment_api_client.dart';
import 'package:meta/meta.dart';

class PaymentRepository {
  final PaymentApiClient paymentApiClient;

  PaymentRepository({@required this.paymentApiClient}) : assert(paymentApiClient != null);

  Future generatePayment(var body) {
    return paymentApiClient.generatePayment(body);
  }

  Future<String> getUserData(String userID) {
    return paymentApiClient.getUserData(userID);
  }

  Future getCommerceData(String codigoPay) {
    return paymentApiClient.getCommerceData(codigoPay);
  }
}
