import 'package:amipay/singleton/app_data.dart';
import 'package:flutter/material.dart';
import 'dart:js' as js;

import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Amipass Pago rapido',
      theme: ThemeData(primarySwatch: Colors.red, fontFamily: 'Poppins'),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  static final _formKey = GlobalKey<FormState>();
  final _commerceController = TextEditingController();
  final _montoController = TextEditingController();
  final _userIDController = TextEditingController();
  final _passwordController = TextEditingController();
  bool isMontoenabled = true;
  String establecimiento = "";
  String local = "";
  String localTemp = "";
  String codigoPay = "";

  bool isLoading = false;

  @override
  void initState() {
    getQueryParameters();
    super.initState();
  }

  getQueryParameters() async {
    var uri = Uri.tryParse(js.context['location']['href']);
    if (uri != null) {
      if (uri.queryParameters['comercio'] != null) {
        if (uri.queryParameters['comercio'].length == 8) {
          setState(() {
            establecimiento = uri.queryParameters['comercio'].substring(0, 5);
            localTemp = uri.queryParameters['comercio'].substring(6);
            local = "0${uri.queryParameters['comercio'].substring(6)}";
            _commerceController.text = "$establecimiento$localTemp";
          });
        } else if (uri.queryParameters['comercio'].length == 7) {
          appData.paymentRepository.getCommerceData(uri.queryParameters['comercio']).then((data) {
            setState(() {
              if (data['comercio'] != null) {
                _commerceController.text = "${data['comercio']}${data['local'].substring(1)}";
                establecimiento = data['comercio'];
                local = data['local'];
              } else {
                _showErrorCodigoPay();
              }
            });
          });
        }
      }

      if (uri.queryParameters['codigopay'] != null) {
        appData.paymentRepository.getCommerceData(uri.queryParameters['codigopay']).then((data) {
          setState(() {
            if (data['comercio'] != null) {
              _commerceController.text = "${data['comercio']}${data['local'].substring(1)}";
              establecimiento = data['comercio'];
              local = data['local'];
            } else {
              _showErrorCodigoPay();
            }
          });
        });
      }

      if (uri.queryParameters['monto'] != null) {
        isMontoenabled = false;
        _montoController.text = uri.queryParameters['monto'];
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage("assets/img/fondo.png"), fit: BoxFit.cover),
        ),
        child: ListView(
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Image.asset('assets/img/amipass.png', height: 100),
                SizedBox(height: 10),
                Card(
                  elevation: 4,
                  child: Container(
                    width: MediaQuery.of(context).size.width / 1.1,
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: pagoRapidoForm(),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  "Necesitas mas informacion?",
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                ),
                InkWell(
                  onTap: () {
                    js.context.callMethod("open", ["https://api.whatsapp.com/send?phone=56933890580"]);
                  },
                  child: Image.asset('assets/img/wasap.png'),
                ),
                InkWell(
                  onTap: () {
                    js.context.callMethod("open", ["https://www.amipass.com/terminos-condiciones-app-usuario.html"]);
                  },
                  child: Text(
                    "Términos y condiciones",
                    style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  ),
                ),
                SizedBox(height: 20),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget pagoRapidoForm() {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RichText(
                text: TextSpan(
                  style: TextStyle(color: Colors.red, fontSize: 22),
                  children: <TextSpan>[
                    TextSpan(text: 'Formulario pago rápido'),
                    TextSpan(text: ' amiPASS', style: TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Text("Completa los datos para realizar el pago", style: TextStyle(fontSize: 16)),
              SizedBox(height: 10),
              TextFormField(
                enabled: false,
                controller: _commerceController,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.store),
                  labelText: 'Código de comercio',
                ),
                validator: (value) {
                  if (value.isEmpty) return 'Por favor ingresa el identificador del Comercio';
                  return null;
                },
              ),
              SizedBox(height: 10),
              TextFormField(
                enabled: isMontoenabled,
                controller: _montoController,
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[WhitelistingTextInputFormatter.digitsOnly],
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.attach_money),
                  border: OutlineInputBorder(),
                  labelText: 'Monto',
                ),
                validator: (value) {
                  if (value.isEmpty) return 'Por favor ingresa un monto';
                  return null;
                },
              ),
              SizedBox(height: 10),
              TextFormField(
                controller: _userIDController,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.person),
                  border: OutlineInputBorder(),
                  labelText: 'Rut usuario o NN',
                ),
                validator: (value) {
                  if (value.isEmpty) return 'Por favor ingresa un usuario';
                  return null;
                },
              ),
              SizedBox(height: 10),
              TextFormField(
                controller: _passwordController,
                decoration: InputDecoration(
                  prefixIcon: Icon(Icons.vpn_key),
                  border: OutlineInputBorder(),
                  labelText: 'Clave de Pago',
                ),
                obscureText: true,
                validator: (value) {
                  if (value.isEmpty) return 'Por favor ingresa una contraseña';
                  return null;
                },
              ),
              SizedBox(height: 20),
              isLoading
                  ? CircularProgressIndicator
                  : RaisedButton(
                      color: Colors.red,
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          setState(() {
                            isLoading = true;
                          });
                          appData.paymentRepository
                              .getUserData(_userIDController.text.replaceAll('.', '').replaceAll('-', ""))
                              .then((sEmpresa) {
                            var body = {
                              "sEmpleado": _userIDController.text.replaceAll('.', '').replaceAll('-', ""),
                              "sEmpresa": sEmpresa,
                              "sClave": _passwordController.text,
                              "sEstablecimiento": establecimiento,
                              "sLocal": local,
                              "nMonto": _montoController.text.replaceAll('.', '')
                            };

                            appData.paymentRepository.generatePayment(body).then((response) {
                              _showDialog(response);
                              setState(() {
                                isLoading = false;
                              });
                              if (response) {
                                setState(() {
                                  _userIDController.text = "";
                                  _passwordController.text = "";
                                  _montoController.text = "";
                                });
                              }
                            });
                          });
                        }
                      },
                      child: isLoading
                          ? Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: CircularProgressIndicator(backgroundColor: Colors.white),
                            )
                          : Text('PAGAR', style: TextStyle(color: Colors.white)),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  void _showDialog(bool isValid) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(isValid ? "Transacción realizada con exito" : "Fallo la transaccion"),
              Image.asset(isValid ? 'assets/img/done.png' : 'assets/img/error.png', height: 100),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("CERRAR"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void _showErrorCodigoPay() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          content: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("El codigo Pay no es valido"),
              Image.asset('assets/img/error.png', height: 100),
            ],
          ),
          actions: <Widget>[
            new FlatButton(
              child: new Text("CERRAR"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
