import 'package:amipay/repositories/payment/payment_api_client.dart';
import 'package:amipay/repositories/payment/payment_repository.dart';
import 'package:http/http.dart' as http;

class AppData {
  static final AppData _appData = new AppData._internal();
  factory AppData() {
    return _appData;
  }
  AppData._internal();

  PaymentRepository paymentRepository = PaymentRepository(
    paymentApiClient: PaymentApiClient(httpClient: http.Client()),
  );
}

final appData = AppData();
